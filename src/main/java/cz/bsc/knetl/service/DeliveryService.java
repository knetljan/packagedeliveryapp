package cz.bsc.knetl.service;

import cz.bsc.knetl.bo.Delivery;
import cz.bsc.knetl.bo.Price;
import cz.bsc.knetl.dao.DeliveryDao;
import cz.bsc.knetl.dao.DeliveryFileDao;
import cz.bsc.knetl.dao.PriceDao;
import cz.bsc.knetl.dao.PriceFileDao;
import cz.bsc.knetl.exceptions.InitialLoadFileReadException;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class DeliveryService {

    private final static Logger logger =  LogManager.getLogger(DeliveryService.class);

    private DeliveryDao deliveryDao;
    private DeliveryFileDao deliveryFileDao;
    private PriceDao priceDao;
    private PriceFileDao priceFileDao;

    public DeliveryService(
            DeliveryDao deliveryDao,
            DeliveryFileDao deliveryFileDao,
            PriceDao priceDao,
            PriceFileDao priceFileDao
    ) {
        this.deliveryDao = deliveryDao;
        this.deliveryFileDao = deliveryFileDao;
        this.priceDao = priceDao;
        this.priceFileDao = priceFileDao;
    }

    private DeliveryService() {}

    /**
     * Save initial load package information from file
     *
     * @param fileName - name of initial load
     * @return  true when success
     * */
    public Boolean bulkSaveFromFile(final String fileName){

        try{
            List<Delivery> deliveriesFromFile = deliveryFileDao.findAll(fileName);
            deliveriesFromFile.stream().forEach(this::create);
            return true;
        }catch (InitialLoadFileReadException ex){
            logger.error("Cannot save package info from file.", ex );
            return false;
        }

    }

    /**
     * Create new delivery entry identified by uuid string value
     *
     * @param delivery - delivery entry
     * @return newly created delivery entry with uuid
     * */
    public Delivery create(final Delivery delivery){
        UUID uuid = UUID.randomUUID();
        delivery.setUUID(uuid.toString());

        BigDecimal weight = delivery.getWeight();
        delivery.setPrice(findPrice(weight));

        deliveryDao.saveOrUpdate(delivery);
        return delivery;
    }

    /**
     *  List all delivery entries
     *
     * @return delivery list
     *
     * */
    public List<Delivery> listAllByWeightDesc(){
        List<Delivery> deliveries = deliveryDao.listAll();
        Collections.sort(deliveries);
        Collections.reverse(deliveries);
        return deliveries;
    }


    /**
     * Save price list from file
     *
     * @param fileName - name of price list
     * @return  true when success
     * */
    public Boolean savePricesFromFile(final String fileName){

        try{
            List<Price> pricesFromFile = priceFileDao.findAll(fileName);
            pricesFromFile.forEach(priceDao::saveOrUpdate);
            return true;
        }catch (InitialLoadFileReadException ex){
            logger.error("Cannot save price list from file.", ex );
            return false;
        }

    }

    /**
     * Find price by package weight
     *
     * @param weight - weight of packate
     * @return  price or null
     * */
    public BigDecimal findPrice(final BigDecimal weight){
        List<Price> sortedPricesByWeight = priceDao.listAllDesc();

        if( sortedPricesByWeight == null || sortedPricesByWeight.isEmpty()){
            // no priceList loaded
            return null;
        }

        for(Price p : sortedPricesByWeight){
            if(weight.compareTo(p.getWeight()) == 0 || weight.compareTo(p.getWeight()) == 1){
                // return price by weight
                return p.getPrice();
            }
        }

        // return the lowest price
        return sortedPricesByWeight.get(sortedPricesByWeight.size() - 1).getPrice();
    }

}
