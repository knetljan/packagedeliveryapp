package cz.bsc.knetl;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import cz.bsc.knetl.bo.Delivery;
import cz.bsc.knetl.dao.DeliveryDao;
import cz.bsc.knetl.dao.DeliveryFileDao;
import cz.bsc.knetl.dao.PriceDao;
import cz.bsc.knetl.dao.PriceFileDao;
import cz.bsc.knetl.exceptions.DeliveryParserException;
import cz.bsc.knetl.job.DeliveryPrintJob;
import cz.bsc.knetl.parser.DeliveryParser;
import cz.bsc.knetl.service.DeliveryService;
import org.apache.commons.lang3.StringUtils;

import java.util.Scanner;
import java.util.Timer;

public class Main {

    private DeliveryDao deliveryDao;
    private DeliveryFileDao deliveryFileDao;
    private PriceDao priceDao;
    private PriceFileDao priceFileDao;

    public Main() {
        this.deliveryDao = new DeliveryDao();
        this.deliveryFileDao = new DeliveryFileDao();
        this.priceDao = new PriceDao();
        this.priceFileDao = new PriceFileDao();
    }

    private static final String EXIT_COMMAND = "quit";

    @Parameter(names={"-initfilename"})
    private String initFileName;

    @Parameter(names={"-pricefilename"})
    private String priceFileName;

    public static void main(String[] args) {
        Main main = new Main();
        JCommander.newBuilder()
                .addObject(main)
                .build()
                .parse(args);
        main.run();
    }

    public void run() {
        DeliveryService deliveryService = new DeliveryService(
                deliveryDao,
                deliveryFileDao,
                priceDao,
                priceFileDao
        );

        // price list load
        if(!StringUtils.isEmpty(priceFileName)){
            deliveryService.savePricesFromFile(priceFileName);
        }

        // initial load
        if(!StringUtils.isEmpty(initFileName)){
            deliveryService.bulkSaveFromFile(initFileName);
        }

        // start printing job
        // print deliveries every minute
        Timer timer = new Timer();
        timer.schedule(new DeliveryPrintJob(deliveryService), 0, 60000);

        // read entries from command line
        for(;;){
            Scanner s = new Scanner(System.in);
            String inputLine = s.nextLine();

            // exit command
            if(!StringUtils.isEmpty(inputLine) && inputLine.equalsIgnoreCase(EXIT_COMMAND)){
                System.exit(0);
            }

            try {
                Delivery delivery = DeliveryParser.parse(inputLine);
                deliveryService.create(delivery);
            } catch (DeliveryParserException e) {
                System.err.println("Could not parse input text. Please insert weight and zip code in the proper format. Please read readme file.");
            }
        }
    }
}
