package cz.bsc.knetl.job;

import cz.bsc.knetl.bo.Delivery;
import cz.bsc.knetl.formatter.DeliveryFormatter;
import cz.bsc.knetl.service.DeliveryService;

import java.util.List;
import java.util.TimerTask;

/**
 * Print all deliveries job
 * */
public class DeliveryPrintJob extends TimerTask {

    private DeliveryService deliveryService;

    public DeliveryPrintJob(DeliveryService deliveryService) {
        this.deliveryService = deliveryService;
    }

    public void run() {

        System.out.println("Postal code | Weight | Price");
        List<Delivery> deliveryList = deliveryService.listAllByWeightDesc();
        deliveryList.forEach(delivery -> {
            System.out.println(DeliveryFormatter.format(delivery));
        });
    }
}
