package cz.bsc.knetl.dao;

import cz.bsc.knetl.bo.Price;
import cz.bsc.knetl.storage.MemoryStorage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class PriceDao {

    public PriceDao() {}

    /**
     * Save or update price entry
     *
     * @param price - price entry
     * */
    public void saveOrUpdate(final Price price){
        MemoryStorage.getPriceStorage().put(price.getWeight(), price);
    }

    /**
     *  List all prices DESC
     *
     * @return price list
     *
     * */
    public List<Price> listAllDesc(){
        List<Price> priceList= MemoryStorage.getPriceStorage()
                .values()
                .stream()
                .collect(Collectors.toCollection(ArrayList::new));

        Collections.sort(priceList);
        Collections.reverse(priceList);
        return priceList;
    }
}
