package cz.bsc.knetl.dao;

import cz.bsc.knetl.bo.Delivery;
import cz.bsc.knetl.storage.MemoryStorage;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * Manipulates with package deliveries in memory storage
 *
 * */
public class DeliveryDao {

    public DeliveryDao() {}

    /**
     * Save or update delivery entry
     *
     * @param delivery - delivery entry
     * */
    public void saveOrUpdate(final Delivery delivery) {
        MemoryStorage.getDeliveryStorage().put(delivery.getUUID(), delivery);
    }

    /**
     *  List all delivery entries
     *
     * @return delivery list
     *
     * */
    public List<Delivery> listAll() {
        return MemoryStorage.getDeliveryStorage().
                values().
                stream().
                collect(Collectors.toCollection(ArrayList::new));
    }

}
