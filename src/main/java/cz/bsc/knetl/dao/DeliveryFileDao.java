package cz.bsc.knetl.dao;

import cz.bsc.knetl.bo.Delivery;
import cz.bsc.knetl.exceptions.DeliveryParserException;
import cz.bsc.knetl.exceptions.InitialLoadFileReadException;
import cz.bsc.knetl.parser.DeliveryParser;
import cz.bsc.knetl.service.DeliveryService;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.util.StreamUtils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Manipulates with package deliveries in the file storage
 */
public class DeliveryFileDao {

    private final static Logger logger =  LogManager.getLogger(DeliveryService.class);

    public DeliveryFileDao() {}

    /**
     *  Read all initial Package Delivery
     *
     * @param fileName - name of initial load
     * @return list of package deliveries
     * @throws  {@link InitialLoadFileReadException}
     * */
    public List<Delivery> findAll(final String fileName) {

        List<Delivery> deliveries = new ArrayList<>();

        try {
            //BufferedReader reader = new BufferedReader(new FileReader(fileName));


            InputStream stream = Files.newInputStream(Paths.get(fileName));
            String[] lines = StreamUtils.copyToString(stream, StandardCharsets.UTF_8).split("\\r?\\n");
            Arrays.asList(lines).forEach(
                    line -> {
                        if (!StringUtils.isEmpty(line)) {
                            try {
                                Delivery delivery = DeliveryParser.parse(line);
                                deliveries.add(delivery);
                            } catch (DeliveryParserException e) {
                                logger.error("Could not parse line in the delivery file. Line :" + line);
                            }
                        }
                    });
        } catch (FileNotFoundException e) {
            logger.error("Could not read the delivery file. File not found. Filename: " + fileName);
            throw new InitialLoadFileReadException();
        } catch (IOException e) {
            logger.error("Could not read the delivery file. Filename: " + fileName + " ex:", e);
            throw new InitialLoadFileReadException();
        }

        return deliveries;
    }
}
