package cz.bsc.knetl.dao;

import cz.bsc.knetl.bo.Price;
import cz.bsc.knetl.exceptions.InitialLoadFileReadException;
import cz.bsc.knetl.exceptions.PriceParserException;
import cz.bsc.knetl.parser.PriceParser;
import cz.bsc.knetl.service.DeliveryService;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.util.StreamUtils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PriceFileDao {

    private final static Logger logger =  LogManager.getLogger(DeliveryService.class);

    public PriceFileDao() {}

    /**
     *  Read all delivery prices
     *
     * @param fileName - name of initial load
     * @return list of prices
     * @throws  {@link InitialLoadFileReadException}
     * */
    public List<Price> findAll(final String fileName) {

        List<Price> priceList = new ArrayList<>();

        try(InputStream stream = Files.newInputStream(Paths.get(fileName))) {
            String[] lines = StreamUtils.copyToString(stream, StandardCharsets.UTF_8).split("\\r?\\n");
            Arrays.asList(lines).forEach(line -> {
                if (!StringUtils.isEmpty(line)) {
                    try{
                        Price price = PriceParser.parse(line);
                        priceList.add(price);
                    }catch (PriceParserException ex){
                        logger.error("Could not parse line in the price file. Line: " + line);
                    }
                }
            });

        } catch (FileNotFoundException e) {
            logger.error("Could not read the price file. File not found. Filename: " + fileName);
            throw new InitialLoadFileReadException();
        } catch (IOException e) {
            logger.error("Could not read the price file store. Filename: " + fileName + " ex:", e);
            throw new InitialLoadFileReadException();
        }

        return priceList;
    }
}
