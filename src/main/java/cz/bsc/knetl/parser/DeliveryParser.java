package cz.bsc.knetl.parser;

import cz.bsc.knetl.bo.Delivery;
import cz.bsc.knetl.exceptions.DeliveryParserException;
import cz.bsc.knetl.service.DeliveryService;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.math.BigDecimal;
import java.util.Set;

public class DeliveryParser {

    private final static Logger logger =  LogManager.getLogger(DeliveryService.class);
    private static final int ENTRIES_PER_ROW = 2;

    private DeliveryParser() {
    }

    public static Delivery parse(String textInput) throws DeliveryParserException {

        if (!StringUtils.isEmpty(textInput)) {
            String[] columns = textInput.split(" ");
            if (columns.length != ENTRIES_PER_ROW) {
                logger.error(
                        "Could not parse line with the delivery entry. There is not valid count of columns. Text: "
                                + textInput);
                throw new DeliveryParserException();
            }

            try {

                Delivery delivery = new Delivery();
                delivery.setWeight(new BigDecimal(columns[0]));
                delivery.setPostalCode(columns[1]);

                // Validating inputs from file
                Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
                Set<ConstraintViolation<Delivery>> violations = validator.validate(delivery);
                if (!violations.isEmpty()) {
                    violations.
                            stream().forEach(v -> logger.error("Could not parse line from the delivery file. error: "
                            + v.getMessage()));
                    throw new DeliveryParserException();
                }

                return delivery;
            } catch (Exception ex) {
                logger.error("Could not parse line from the delivery file. " +
                        "There is not valid weight or postal code value format. Line: " + textInput);
                throw new DeliveryParserException();
            }
        }
        logger.error("Could not parse line from the delivery file. Input text is empty");
        throw new DeliveryParserException();
    }

}
