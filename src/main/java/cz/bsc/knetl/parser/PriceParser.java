package cz.bsc.knetl.parser;

import cz.bsc.knetl.bo.Price;
import cz.bsc.knetl.exceptions.PriceParserException;
import cz.bsc.knetl.service.DeliveryService;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.math.BigDecimal;
import java.util.Set;

public class PriceParser {

    private final static Logger logger =  LogManager.getLogger(DeliveryService.class);
    private static final int ENTRIES_PER_ROW = 2;

    private PriceParser() {
    }

    public static Price parse(String textInput) throws PriceParserException {

        if (!StringUtils.isEmpty(textInput)) {
            String[] columns = textInput.split(" ");
            if (columns.length != ENTRIES_PER_ROW) {
                logger.error("Could not parse line with the price entry. There is not valid count of columns. Text: "
                        + textInput);
                throw new PriceParserException();
            }

            try {

                // Validating inputs from file
                Price price = new Price(new BigDecimal(columns[0]), new BigDecimal(columns[1]));
                Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
                Set<ConstraintViolation<Price>> violations = validator.validate(price);
                if (!violations.isEmpty()) {
                    violations.stream().forEach(v -> logger.error("Could not parse line from the price file. error: "
                            + v.getMessage()));
                    throw new PriceParserException();
                }

                return price;
            } catch (Exception ex) {
                logger.error("Could not parse line from the price file. " +
                        "There is not valid weight or price value format. Line: "
                        + textInput);
                throw new PriceParserException();
            }
        }
        logger.error("Could not parse line in the price file. Input text is empty.");
        throw new PriceParserException();
    }

}
