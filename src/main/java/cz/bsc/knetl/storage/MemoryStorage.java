package cz.bsc.knetl.storage;

import cz.bsc.knetl.bo.Delivery;
import cz.bsc.knetl.bo.Price;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * Delivery storage in memory
 * UUID (String) is unique identifier of the delivery entry
 *
 * */
public class MemoryStorage {

    private MemoryStorage() {};

    private static final ConcurrentHashMap<String, Delivery> deliveryStorage = new ConcurrentHashMap<>();

    // Read only after init load
    private static final Map<BigDecimal, Price> priceStorage = new HashMap<>();

    public static ConcurrentHashMap<String, Delivery> getDeliveryStorage() {
        return deliveryStorage;
    }

    public static Map<BigDecimal, Price> getPriceStorage() {
        return priceStorage;
    }
}
